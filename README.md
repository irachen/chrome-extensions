# 弹幕抓手

开发浏览器插件，请先阅读[官方指南](https://developer.chrome.com/docs/extensions/mv2/getstarted/)

## 淘宝

Manifest 中写明了，插件仅在 https://liveplatform.taobao.com/* 有效

## 快手

Manifest 中写明了，插件仅在 https://live.kuaishou.com/* 有效

## 安装

按[说明](https://developer.chrome.com/docs/extensions/mv2/getstarted/#manifest)在开发者模式载入当前目录即可
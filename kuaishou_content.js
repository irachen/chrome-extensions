console.log('contentScript start');

const config = { attributes: false, childList: true, subtree: true };

const callback = function (mutationsList, observer) {
  for (let mutation of mutationsList) {
    if (mutation.type === 'childList') {
      mutation.addedNodes.forEach(function (node) {
        if (node.querySelector) {
          const nickname = node.querySelector('.username').textContent;
          const comment = node.querySelector('.comment').textContent;
          const giftImage = node.querySelector('.gift-img');
          // TODO
          console.log({
            nickname,
            comment,
            img: giftImage ? giftImage.src : '',
          });
          chrome.runtime.sendMessage({
            nickname,
            comment,
            img: giftImage ? giftImage.src : '',
          }, function (response) {
            console.log(response);
          });
        }
      })
    }
    else if (mutation.type === 'attributes') {
      console.log('The ' + mutation.attributeName + ' attribute was modified.');
    }
  }
};

// Create an observer instance linked to the callback function
const observer = new MutationObserver(callback);

// Start observing the target node for configured mutations
observer.observe(document.querySelector('.chat-history'), config);

console.log('contentScript end');
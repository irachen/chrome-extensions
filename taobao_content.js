console.log('contentScript start');

const config = { attributes: false, childList: true, subtree: true };

// Callback function to execute when mutations are observed
const callback = function (mutationsList, observer) {
  // Use traditional 'for loops' for IE 11
  for (let mutation of mutationsList) {
    if (mutation.type === 'childList') {
      mutation.addedNodes.forEach(function (parent) {
        if (parent.className === 'comment-list') {
          const nickname = parent.querySelector('.cl-info').textContent;
          const comment = parent.querySelector('.cl-content').textContent;
          // presentationConnection.send(JSON.stringify({ nickname, comment, message: enums[cnt++], lang: 'en-US' }));
          console.log({
            nickname,
            comment,
          });
          chrome.runtime.sendMessage({
            nickname,
            comment,
          }, function (response) {
            console.log(response);
          });
        } else {
          parent.querySelectorAll('.comment-list').forEach(node => {
            const nickname = node.querySelector('.cl-info').textContent;
            const comment = node.querySelector('.cl-content').textContent;
            // presentationConnection.send(JSON.stringify({ nickname, comment, message: enums[cnt++], lang: 'en-US' }));
            console.log({
              nickname,
              comment,
            });
            chrome.runtime.sendMessage({
              nickname,
              comment,
            }, function (response) {
              console.log(response);
            });
          })
        }
      })
    }
    else if (mutation.type === 'attributes') {
      console.log('The ' + mutation.attributeName + ' attribute was modified.');
    }
  }
};

// Create an observer instance linked to the callback function
const observer = new MutationObserver(callback);

// Start observing the target node for configured mutations
observer.observe(document.querySelector('.panel-comment .next-tabs-content'), config);

console.log('contentScript end');
'use strict';

chrome.runtime.onInstalled.addListener(function () {
    console.log('Installed');
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
        chrome.declarativeContent.onPageChanged.addRules([{
            conditions: [new chrome.declarativeContent.PageStateMatcher({
                pageUrl: { hostContains: 'live' },
            })
            ],
            actions: [new chrome.declarativeContent.ShowPageAction()]
        }]);
    });
});

chrome.pageAction.onClicked.addListener(function () {
    chrome.tabs.create({ url: "receiver.html" }, function (tab) {
    });
});

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    chrome.tabs.query({
        url: '*://*/receiver.html',
    }, function (tabs) {
        if (tabs.length) {
            chrome.tabs.sendMessage(tabs[0].id, request, function (response) {
            });
        }
    });
});
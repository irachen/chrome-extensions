function addComment(obj) {
  const listItem = document.createElement("li");
  const nickname = document.createElement("span");
  const comment = document.createElement("span");
  nickname.className = "nickname";
  comment.className = "comment";
  nickname.textContent = obj.nickname;
  comment.textContent = obj.comment;
  listItem.append(nickname, comment);

  if (obj.img) {
    const giftImage = document.createElement("img");
    giftImage.src = obj.img;
    giftImage.className = "gift-img";
    listItem.append(giftImage);
  }

  document.querySelector("#message-list").appendChild(listItem);
  window.scrollTo(0, document.body.scrollHeight);
}

document.addEventListener('DOMContentLoaded', function () {
  chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    console.log(request);
    addComment(request);
  });
});
console.log('contentScript start');

const log = console.log;
const presentationRequest = new PresentationRequest(['https://cnvb-vd0.oss-cn-hangzhou.aliyuncs.com/irachen/index.html']);

navigator.presentation.defaultRequest = presentationRequest;

let presentationConnection;

presentationRequest.addEventListener('connectionavailable', function (event) {
  presentationConnection = event.connection;
  presentationConnection.addEventListener('close', function () {
    log('> Connection closed.');
  });
  presentationConnection.addEventListener('terminate', function () {
    log('> Connection terminated.');
  });
  presentationConnection.addEventListener('message', function (event) {
    log('> ' + event.data);
  });
});

presentationRequest.getAvailability()
  .then(availability => {
    log('Available presentation displays: ' + availability.value);
    availability.addEventListener('change', function () {
      log('> Available presentation displays: ' + availability.value);
    });
  })
  .catch(error => {
    log('Presentation availability not supported, ' + error.name + ': ' +
      error.message);
  });

// TODO 监听 DOM change
var cnt = 1;
var enums = ['grapes', 'watermelon', 'melon', 'tangerine'];
// Options for the observer (which mutations to observe)
const config = { attributes: false, childList: true, subtree: true };

// Callback function to execute when mutations are observed
const callback = function (mutationsList, observer) {
  // Use traditional 'for loops' for IE 11
  for (let mutation of mutationsList) {
    if (mutation.type === 'childList') {
      mutation.addedNodes.forEach(function (node) {
        const nickname = node.querySelector('.username').textContent;
        const comment = node.querySelector('.comment').textContent;
        presentationConnection.send(JSON.stringify({ nickname, comment, message: enums[cnt++], lang: 'en-US' }));
      })
    }
    else if (mutation.type === 'attributes') {
      console.log('The ' + mutation.attributeName + ' attribute was modified.');
    }
  }
};

// Create an observer instance linked to the callback function
const observer = new MutationObserver(callback);


chrome.runtime.onMessage.addListener(
  function (request, sender, sendResponse) {
    console.log(sender.tab ?
      "from a content script:" + sender.tab.url :
      "from the extension");

    presentationRequest.start()
      .then(connection => {
        log('> Connected to ' + connection.url + ', id: ' + connection.id);
        sendResponse({ status: 0 });
      })
      .catch(error => {
        log('> ' + error.name + ': ' + error.message);
      });

    // Start observing the target node for configured mutations
    observer.observe(document.querySelector('.chat-history'), config);

    return true;
  }
)

// Later, you can stop observing
// observer.disconnect();
// setTimeout(function () {
//   console.log('post apple');
//   presentationConnection.send(JSON.stringify({ message: 'apple', lang: 'en-US' }));
// }, 10000);

console.log('contentScript end');